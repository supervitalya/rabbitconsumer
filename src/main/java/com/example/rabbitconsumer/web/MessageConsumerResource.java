package com.example.rabbitconsumer.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.rabbitconsumer.service.RabbitConsumerService;
import lombok.extern.java.Log;

/**
 * Created by Vitali_Bliakharchuk on 3/21/2018.
 */
@Log
@CrossOrigin
@RestController
@RequestMapping("/messages")
public class MessageConsumerResource {

    private final RabbitConsumerService rabbitConsumerService;

    public MessageConsumerResource(RabbitConsumerService rabbitConsumerService) {
        this.rabbitConsumerService = rabbitConsumerService;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, path = "/{severity}")
    public String getMessage(@PathVariable String severity) {
        log.info("Request for getting message from queue");
        return rabbitConsumerService.getMessage(severity);
    }
}
