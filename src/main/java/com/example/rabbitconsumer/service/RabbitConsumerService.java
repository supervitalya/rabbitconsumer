package com.example.rabbitconsumer.service;

/**
 * Created by Vitali_Bliakharchuk on 3/21/2018.
 */
public interface RabbitConsumerService {

    String getMessage(String severity);
}
