package com.example.rabbitconsumer.service;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import lombok.extern.java.Log;

/**
 * Created by Vitali_Bliakharchuk on 3/21/2018.
 */
@Service
@Log
public class RabbitConsumerServiceImpl implements RabbitConsumerService {

    @Value("${app.subscribingTimeout}")
    private int timeout = 10;

    @Value("${app.host}")
    private String host;

    @Value("${app.exchangeType}")
    private String exchangeType;

    @Value("${app.exchangeName}")
    private String exchangeName;

    @Override
    public String getMessage(String severity) {
        log.info("Getting message from queue.");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        Queue<String> messageContainer = new ArrayDeque<>(1);
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.exchangeDeclare(exchangeName, exchangeType);
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, exchangeName, severity);
            log.info(String.format("Queue name: %s.", queueName));
            channel.basicConsume(queueName, true, createRabbitConsumer(channel, messageContainer));
            // Subscribe for a few seconds. Just for testing purpose.
            log.info(String.format("Subscribed on messages with severity %s, for %d seconds", severity, timeout));
            TimeUnit.SECONDS.sleep(timeout);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return messageContainer.poll();
    }

    private Consumer createRabbitConsumer(Channel channel, Queue<String> messageContainer) {
        return new DefaultConsumer(channel) {

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String receivedMessage = new String(body, "UTF-8");
                log.info("Received message: " + receivedMessage);
            }
        };
    }
}
